//Diganta Das
//28/06/2015
//This class contains the database connection variables
//use variable as shown in the main function below

package project_path.Helper_Class;

//import com.mysql.jdbc.Connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB_Con {
	
	public Connection conn;
	public ResultSet rs = null;
	public Statement stmt=null;
	
	public DB_Con(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/"+G_Strings.db_name;
			String connectionUser = G_Strings.db_user;
			String connectionPassword = G_Strings.db_password;
			
			conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void close_db(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ResultSet exec(String st){
		try{
			stmt = conn.createStatement();
			rs = stmt.executeQuery(st);
		}catch(Exception e){
			e.printStackTrace();
		}
		return rs;
	}
	// method shows how to use the cals object to call database
	public static void main(String[] args) {
		
		DB_Con db=new DB_Con();
		
		try{
			//Statement stmt = db.conn.createStatement();
			//ResultSet rs = stmt.executeQuery("SELECT * FROM user");
			String username="1425913";
			String password="67759906";
			
			ResultSet res=db.exec("Select * from User where user_name=1425913");
			
			//while (res.next()) {
				//String id = res.getString("id");
				//String firstName = res.getString("name");
				//System.out.println("ID: " + id + ", First Name: " + firstName);
				//String firstName = res.getString("name");
				//System.out.println(firstName);
			//}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		db.close_db();
	}

}
