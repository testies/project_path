package project_path.Helper_Class;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class User_Helper {
	
	
	DB_Con db=new DB_Con();
	
	public static void main(String arg[]){
		User_Helper u_help=new User_Helper();
		//u_help.search_member("1425913");
	}
	
	public ResultSet search_member(String search_param,String pro_id){
		if(search_param.equals("")){
			search_param="----";
		}
		System.out.println("Search pro ID : "+pro_id);
		try {
			search_param="%"+search_param+"%";
			PreparedStatement stmt=db.conn.prepareStatement("Select user_id,user_name,name,course,email_id,photo_link from user where (name like ? OR user_name like ? OR email_id like ?) and (user_id NOT IN (Select user_id from project_link where project_id=?))");
			stmt.setString(1, search_param);
			stmt.setString(2, search_param);
			stmt.setString(3, search_param);
			stmt.setString(4, pro_id);
			
			ResultSet result=stmt.executeQuery();
			
			//while(result.next()){
			//	System.out.println(result.getString("name"));
			//}
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("User_Class get_current Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
}
