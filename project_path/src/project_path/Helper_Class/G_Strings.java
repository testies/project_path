//Diganta Das
//28/06/2015
//This file should contain all global strings
//please add you static strings when working

package project_path.Helper_Class;

public class G_Strings {
	public static String db_name="projectpath";
	public static String db_user="root";
	public static String db_password="";
	public static String url="http://localhost:8081/project_path";
	public static String file_uploads="e:\\projects\\pp_uploads\\files\\";
	public static String dp_uploads="e:\\projects\\pp_uploads\\dp_images\\";
	public static String footer="Anand Srivastava, Diganta Das, Jonathan Paul (MCA 2014-17), Christ University";
}
