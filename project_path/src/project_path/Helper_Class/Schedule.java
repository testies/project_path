package project_path.Helper_Class;

public class Schedule
{
	public int length,max_length;
	public String phase_names[];
	public String start_dates[];
	public String end_dates[];
	public String descriptions[];
	
	public Schedule(int n)
	{
		max_length=n;
		phase_names=new String[max_length];
		start_dates=new String[max_length];
		end_dates=new String[max_length];
		descriptions=new String[max_length];
	}
	public void addData(String phase_name,String start_date,String end_date,String description)
	{
		if(length<max_length)
		{
			phase_names[length]=phase_name;
			start_dates[length]=start_date;
			end_dates[length]=end_date;
			descriptions[length++]=description;
		}
	}
	public void show()
	{
		System.out.println(length);
		for(int i=0;i<length;i++)
		{
			System.out.println(phase_names[i]);
			System.out.println(start_dates[i]);
			System.out.println(end_dates[i]);
			System.out.println(descriptions[i]);
		}
	}
}
