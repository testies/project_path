package project_path.Helper_Class;

import java.io.ObjectInputStream.GetField;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;

public class ProjectHelper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProjectHelper pro=new ProjectHelper();
		//pro.get_current_project(3);
		//pro.get_past_project(3);
		//pro.get_project_invites(3);
		//pro.get_project_members(2);
		//pro.project_file_upload("", "", "", "");
		pro.get_project_files("6");

	}
	public ResultSet get_current_project(int user_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("SELECT * FROM project, project_link where project.project_id=project_link.project_id and project_link.user_id=? and project.status=1");
			stmt.setInt(1, user_id);
			
			ResultSet result=stmt.executeQuery();
			
			while(result.next()){
				System.out.println(result.getString("title"));
			}
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_current Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public ResultSet get_past_project(int user_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("SELECT * FROM project, project_link where project.project_id=project_link.project_id and project_link.user_id=? and project.status=2");
			stmt.setInt(1, user_id);
			
			ResultSet result=stmt.executeQuery();
			return result;
			//while(result.next()){
			//	System.out.println(result.getString("title"));
			//}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_past Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public ResultSet get_project_invites(int user_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Select project.project_id, user.name, title,category from project_invitation,user,project where project_invitation.invited_id=? and project_invitation.sender_id=user.user_id and project_invitation.project_id=project.project_id");
			stmt.setInt(1, user_id);
			
			ResultSet result=stmt.executeQuery();
			return result;
			//while(result.next()){
			//	System.out.println(result.getString("title"));
			//}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_invites Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public String get_project_members(int project_id){
		DB_Con db=new DB_Con();
		String member_names="";
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Select user.name from user, project, project_link where project_link.project_id=project.project_id and user.user_id=project_link.user_id and project.project_id=?");
			stmt.setInt(1, project_id);
			
			ResultSet result=stmt.executeQuery();
			while(result.next()){
				member_names=member_names+result.getString("name")+", ";
			}
			//System.out.println(member_names);
			return member_names;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_invites Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public ResultSet get_project_members_set(String pro_id){
		DB_Con db=new DB_Con();
		String member_names="";
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Select user.user_id, user.name, user.photo_link from user, project, project_link where project_link.project_id=project.project_id and user.user_id=project_link.user_id and project.project_id=?");
			stmt.setString(1, pro_id);
			
			ResultSet result=stmt.executeQuery();
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_member_set Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public int isprojectmember(String pro_id, String user_id){
		//System.out.println(pro_id+" "+user_id);
		DB_Con db=new DB_Con();
		int count=0;
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Select project_id from project_link where project_id=? and user_id=?");
			stmt.setString(1, pro_id);
			stmt.setString(2, user_id);
			//System.out.println(stmt.toString());
			ResultSet result=stmt.executeQuery();
			result.last();
			count=result.getRow();
			//System.out.println("Count: "+count);
			return count;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_invites Error : "+e.toString());
			e.printStackTrace();
			return 0;
		}
	}
	public ResultSet get_project_details(String pro_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("SELECT * FROM project where project_id=?");
			stmt.setString(1, pro_id);
			
			ResultSet result=stmt.executeQuery();
			return result;
			//while(result.next()){
			//	System.out.println(result.getString("title"));
			//}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_past Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public int project_file_upload(String pro_id, String user_id, String file_name, String ext){
		DB_Con db=new DB_Con();
		DateFormat df = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
		Date dateobj = new Date();
		int f_id=-1;
		//System.out.println(df.format(dateobj));
		try{
			PreparedStatement stmt=db.conn.prepareStatement("Insert into file_upload(project_id,user_id,date,name,extention) values(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, pro_id);
			stmt.setString(2, user_id);
			stmt.setString(3, df.format(dateobj));
			stmt.setString(4, file_name);
			stmt.setString(5, ext);
			
			stmt.execute();
			ResultSet generatedKeys = stmt.getGeneratedKeys();
			
			if (generatedKeys.next()){
				f_id=generatedKeys.getInt(1);
				System.out.println("File uploaded: "+file_name+", id "+f_id);
			}
		}catch(Exception e){
			System.out.println("Project File Upload Database Insert Error "+e.toString());
			e.printStackTrace();
		}
		return f_id;
	}
	public ResultSet get_project_files(String pro_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("SELECT file_id,file_upload.name as fname,extention,date,user.user_id,user.name as uname FROM file_upload,user where project_id=? and file_upload.user_id=user.user_id");
			stmt.setString(1, pro_id);
			
			ResultSet result=stmt.executeQuery();
			
//			while(result.next()){
//				System.out.println(result.getString("fname"));
//				System.out.println(result.getString("uname"));
//			}
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class get_past Error : "+e.toString());
			e.printStackTrace();
			return null;
		}
	}
	public void invite_members(String pro_id,String inviting_id,String invited_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt2=db.conn.prepareStatement("Select * from project_invitation where project_id=? and invited_id=?");
			stmt2.setString(1, pro_id);
			stmt2.setString(2, invited_id);
			
			ResultSet res=stmt2.executeQuery();
			res.last();
			if(res.getRow()>0){
				return;		//invitation already sent before
			}
			
			PreparedStatement stmt=db.conn.prepareStatement("Insert into project_invitation(project_id,sender_id,invited_id) values(?,?,?)");
			stmt.setString(1, pro_id);
			stmt.setString(2, inviting_id);
			stmt.setString(3, invited_id);
			stmt.execute();
			//ResultSet result=stmt.executeQuery();
			
//			while(result.next()){
//				System.out.println(result.getString("fname"));
//				System.out.println(result.getString("uname"));
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class add member Error : "+e.toString());
			e.printStackTrace();
		}
	}
	public void accept_invite(String user_id, String pro_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Insert into project_link (project_id,user_id) values (?,?)");
			stmt.setString(1, pro_id);
			stmt.setString(2, user_id);
			stmt.execute();
			System.out.println("Invitation Accepted");
			//to remove from project invitation table
			reject_invite(user_id, pro_id);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class remove invite Error : "+e.toString());
			e.printStackTrace();
		}
	}
	public void reject_invite(String user_id, String pro_id){
		DB_Con db=new DB_Con();
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Delete from project_invitation where project_id=? and invited_id=?");
			stmt.setString(1, pro_id);
			stmt.setString(2, user_id);
			stmt.execute();
			System.out.println("Rejected Accepted");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Project_Class remove invite Error : "+e.toString());
			e.printStackTrace();
		}
	}
}
