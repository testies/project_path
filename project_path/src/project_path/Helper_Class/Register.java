/*
 * Jonathan Fidelis Paul
 * 07-08-2015
 * This class Registers a new user
 * */
package project_path.Helper_Class;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import project_path.servelets.login_servlet;

public class Register {
	final String uploadPath="C:\\Users\\SONY\\Desktop\\uploads";
	public void registerNewUser(String user_name,String password,String name,String course,String email,String canvasData,int access_level) throws IOException
	{
		String photo_link=user_name+".png";
		int status=0;
		int theme_color=1;
		DB_Con db=new DB_Con();
		PreparedStatement st;
		System.out.println("Username="+user_name+" Password="+password+"Photo="+photo_link);
		try {
				 DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				 Date date= new Date();
			     
				 st = db.conn.prepareStatement("insert into user (user_name,password,name,course,acess_level,email_id,photo_link,joined_on,status,theme_color) values(?,?,?,?,?,?,?,?,?,?)");
		         st.setString(1,user_name);
		         st.setString(2,password);
		         st.setString(3,name);
		         st.setString(4,course);
		         st.setInt(5,access_level);
		         st.setString(6,email);
		         st.setString(7,photo_link);
		         st.setString(8,df.format(date));
		         st.setInt(9,status);
		         st.setInt(10,theme_color);

		         st.executeUpdate();
		         if(canvasData!=null)
		         imageUpload(canvasData,user_name);
		         st.clearParameters();
			} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void imageUpload(String canvasData,String username)throws IOException
	{
    	byte[] decodedData=decoder64.decode(canvasData);
    	BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedData));
        File outputfile = new File(uploadPath+"\\"+username+".png");
        ImageIO.write(image, "png", outputfile);
	}
}