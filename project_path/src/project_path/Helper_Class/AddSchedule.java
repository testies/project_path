package project_path.Helper_Class;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AddSchedule {
	/*
	public static void main(String[]args) throws SQLException
	{
		//insertIntoDB("The RDBMS Schedule","RDBMS","2015-3","MCA");
		Schedule s=new Schedule(3);
		s.addData("phase1", "1", "15", "ANother");
		s.addData("phase2", "2", "25", "ANother");
		s.addData("phase3", "3", "35", "ANother");
		s.show();
	}*/
	public static void insertIntoDB(String schedule_name,String category,String year_sem,String course,Schedule sch) throws SQLException
	{
		DB_Con db=new DB_Con();
		int user_id=0;
		int p_id=0;
		//System.out.println(schedule_name+" "+category+" "+year_sem+" "+course);
		//sch.show();
		PreparedStatement stmt=db.conn.prepareStatement("Insert into grand_schedule(name,course,category,year_sem,user_id) values(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, schedule_name);
		stmt.setString(2, course);
		stmt.setString(3, category);
		stmt.setString(4, year_sem);
		stmt.setInt(5, user_id);
		
		stmt.execute();
		ResultSet generatedKeys = stmt.getGeneratedKeys();
		if (generatedKeys.next()){
			p_id=generatedKeys.getInt(1);
			System.out.println("Added project "+schedule_name+", id "+p_id);
		}
		
		for(int i=0;i<sch.length;i++)
		{
			PreparedStatement stmt2=db.conn.prepareStatement("Insert into schedule_deadline(schedule_id,phase_name,start_date,end_date,description) values(?,?,?,?,?)");
			stmt2.setInt(1, p_id);
			stmt2.setString(2,sch.phase_names[i]);
			stmt2.setString(3,sch.start_dates[i]);
			stmt2.setString(4,sch.end_dates[i]);
			stmt2.setString(5,sch.descriptions[i]);
			stmt2.execute();
		}
	}
}
