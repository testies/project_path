//Diganta Das
//23/07/2015
//This class contains the add new project code


package project_path.Helper_Class;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddProject {
	public AddProject(){
		
	}
	public void add_project(String name,String desc, String cat, String link, String keys, int userid, String dept){
		DB_Con db=new DB_Con();
		
		DateFormat df = new SimpleDateFormat("dd/MM/yy");
		Date dateobj = new Date();
		int p_id=0;
		//System.out.println(df.format(dateobj));
		
		try {
			PreparedStatement stmt=db.conn.prepareStatement("Insert into project(title,start_date,description,status,course,category,owner_id,github_link) values(?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
			stmt.setString(2, df.format(dateobj));
			stmt.setString(3, desc);
			stmt.setInt(4, 1);
			stmt.setString(5, dept);
			stmt.setString(6, cat);
			stmt.setInt(7, userid);
			stmt.setString(8, link);
			
			stmt.execute();
			ResultSet generatedKeys = stmt.getGeneratedKeys();
			if (generatedKeys.next()){
				p_id=generatedKeys.getInt(1);
				System.out.println("Added project "+name+", id "+p_id);
			}
			PreparedStatement stmt2=db.conn.prepareStatement("Insert into project_link values(?,?)");
			stmt2.setInt(1, p_id);
			stmt2.setInt(2, userid);
			stmt2.execute();
			System.out.println("Project link added");
			
		}catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}
