package project_path.servelets;

import java.io.IOException;
import java.sql.SQLException;
import project_path.Helper_Class.Schedule;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project_path.Helper_Class.AddSchedule;
/**
 * Servlet implementation class set_grand_schedule
 */
@WebServlet("/set_grand_schedule")
public class set_grand_schedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
    Schedule sch;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public set_grand_schedule() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("InPost");
		String schedule_name=request.getParameter("schedule_name");
		String category=request.getParameter("category");
		String year_sem=request.getParameter("year_sem");
		String course=request.getParameter("course");
		int phase_nos=Integer.parseInt(request.getParameter("count"));
		sch=new Schedule(phase_nos);
		System.out.println(phase_nos);
		for(int i=0;i<phase_nos;i++)
		{
			sch.addData(request.getParameter("phase_name"+i),request.getParameter("start_date"+i),request.getParameter("end_date"+i),request.getParameter("description"+i));
		}
		AddSchedule add_s=new AddSchedule();
		try {
			add_s.insertIntoDB(schedule_name,category,year_sem,course,sch);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
