/*
 *Jonathan Fidelis Paul
 * 25-07-2015
 * This servlet takes input from registration.jsp and calls Register.java 
 */
package project_path.servelets;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import project_path.Helper_Class.DB_Con;
import project_path.Helper_Class.G_Strings;
import project_path.Helper_Class.decoder64;

import com.mysql.jdbc.Connection;

import project_path.Helper_Class.Register;
/**
 * Servlet implementation class registration_servlet
 */
@WebServlet(description = "class to register new users", urlPatterns = { "/registration_servlet" })
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB

public class registration_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public registration_servlet() {
        super();
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
		String user_name=request.getParameter("reg_no");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String course=request.getParameter("course");
		String name=request.getParameter("name");
		String canvasData=request.getParameter("canvasData");
    	
		new Register().registerNewUser(user_name,password,name,course,email,canvasData,0/*access_level*/);
        //Login 
        login_servlet.login(request, response,user_name,password);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void imageUpload(String canvasData,String username)throws IOException
	{
    	byte[] decodedData=decoder64.decode(canvasData);
    	BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedData));
        File outputfile = new File(G_Strings.dp_uploads+username+".png");
        ImageIO.write(image, "png", outputfile);
        System.out.println("Uploaded image");
	}
	
}
