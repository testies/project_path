/*
 * Jonathan Fidelis Paul
 * 07-08-2015
 * Servlet to Add new User by admin
 * */
package project_path.servelets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project_path.Helper_Class.Register;

/**
 * Servlet implementation class add_new_user
 */
@WebServlet(description = "servlet to add privileged user", urlPatterns = { "/add_new_user" })
public class add_new_user extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public add_new_user() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String user_name=request.getParameter("reg_no");
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		String course=request.getParameter("course");
		String access_level_string=request.getParameter("access_level");
		
		int access_level=0;
		if(access_level_string.equals("Teacher"))
			access_level=1;
		if(access_level_string.equals("Project Coordinator"))
			access_level=10;
		if(access_level_string.equals("HOD"))
			access_level=20;
		
		new Register().registerNewUser(user_name,password,name,course,email,"",access_level);
		//TO DO: Redirect to page saying new user has been added
		
		//System.out.println(name+" "+reg_no+" "+email+" "+password+" "+course+" "+access_level);
		
	}

}
