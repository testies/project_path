package project_path.servelets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import project_path.Helper_Class.G_Strings;
import project_path.Helper_Class.ProjectHelper;


@MultipartConfig
/**
 * Servlet implementation class File_Upload
 */
@WebServlet("/File_Upload")

public class File_Upload extends HttpServlet {
	ProjectHelper helper=new ProjectHelper();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public File_Upload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String pro_id=(String)request.getParameter("pro_id");
		String user_id=(String)session.getAttribute("user_id");
		int f_id=-1;
		//System.out.println("From Upload "+pro_id);
		PrintWriter resp=response.getWriter();
		//resp.println("Uploaded");
		
		//System.out.println("Requesting file");
		Part file_part=request.getPart("file");
		String fname=process_filename(file_part);
		String ext=get_extension(fname);
		System.out.println("FIle Ext "+ext);
		if(ext.equals("png")|ext.equals("jpg")|ext.equals("jpeg")|ext.equals("bmp")|ext.equals("doc")|ext.equals("docx")|ext.equals("txt")|ext.equals("pdf")){
		//if(ext.equals("png")){
			System.out.println("file valid");
		}
		else{
			System.out.println("file_invalid");
			resp.println("2");	//invalid fromat
			return;
		}
		if(file_part==null){
			resp.println("0");	//upload failed
			System.out.println("File Null");
		}
		else{
			//System.out.println(file_part.getHeader("content-disposition"));
			//File uploadFolder = new File("c:\\uploads");
			//File uploads =File.createTempFile("image1", "jpg", uploadFolder);
			InputStream istream=file_part.getInputStream();
			f_id=helper.project_file_upload(pro_id, user_id, fname, ext);
			file_part.write(G_Strings.file_uploads+f_id+"."+ext);
			resp.println("1");	//upload succeded
		}
	}
	private static String process_filename(Part part){
		String fname=null;
		for (String cd : part.getHeader("content-disposition").split(";")){
			if (cd.trim().startsWith("filename")){
				fname=cd.substring(cd.indexOf('=')+1).trim().replace("\"","");
				System.out.println(fname);
			}
		}
		return fname;
	}
	private static String get_extension(String fname){
		String ext=fname.substring(fname.indexOf('.')+1).trim();
		return ext;
	}
}
