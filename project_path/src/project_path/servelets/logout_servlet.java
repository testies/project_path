//by Diganta Das on 21 Jul 2015
//handles logout connection

package project_path.servelets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class logout_servlet
 */
@WebServlet("/logout_servlet")
public class logout_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public logout_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		
		System.out.println(session.getAttribute("name")+" is Looging out");
		session.setAttribute("name", null);
		session.setAttribute("user_id", null);
		session.setAttribute("reg_no", null);
		session.setAttribute("logged_in", "false");
		
		
		response.sendRedirect("/project_path/pages/login_page.jsp");
	}

}
