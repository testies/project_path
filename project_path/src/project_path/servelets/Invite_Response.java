package project_path.servelets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project_path.Helper_Class.ProjectHelper;

/**
 * Servlet implementation class Invite_Response
 */
@WebServlet("/Invite_Response")
public class Invite_Response extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	ProjectHelper pro_help;
    public Invite_Response() {
        super();
        pro_help=new ProjectHelper();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String pro_id=request.getParameter("pro_id");
		String user_id=request.getParameter("user_id");
		int resp=Integer.parseInt(request.getParameter("resp"));
		
		if(resp==0){
			pro_help.reject_invite(user_id, pro_id);
		}
		else{
			pro_help.accept_invite(user_id, pro_id);
		}
	}

}
