package project_path.servelets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;

import project_path.Helper_Class.AddProject;
import project_path.Helper_Class.G_Strings;

/**
 * Servlet implementation class new_project
 */
@WebServlet("/new_project")
public class new_project extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public new_project() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		
		String name=request.getParameter("name");
		String desc=request.getParameter("desc");
		String category=request.getParameter("category");
		String github_link=request.getParameter("github_link");
		String keywords=request.getParameter("keywords");
		int userid=Integer.parseInt((String)session.getAttribute("user_id"));
		String course=(String)session.getAttribute("course");
		
		System.out.println(name+"\n"+desc+"\n"+category+"\n"+github_link+"\n"+keywords);
		
		AddProject add_p=new AddProject();
		add_p.add_project(name, desc, category, github_link, keywords, userid, course);
		
		response.sendRedirect(G_Strings.url+"/pages/myproject_page.jsp");
	}

}
