//by Diganta Das on 21 Jul 2015
//handles login connection

package project_path.servelets;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
//import java.io.PrintWriter;
import java.util.Map;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import project_path.Helper_Class.Register;
import project_path.Helper_Class.login;

/**
 * Servlet implementation class login_servlet
 */
@WebServlet("/login_servlet")
public class login_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public login_servlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			String username = request.getParameter("username");
			String pass = request.getParameter("password");
//Jonathan changed
			login(request,response,username,pass);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	
//Jonathan Added
	public static void login(HttpServletRequest request,HttpServletResponse response,String username,String pass)
	{
		login login_obj=new login();
		try{
			Map<String, String> login_map=login_obj.check_login(username, pass);
			HttpSession session = request.getSession(false);
			if(login_map.get("user_id")!=null){
			//login successful
			session.setAttribute("name", login_map.get("name"));
			session.setAttribute("user_id", login_map.get("user_id"));
			session.setAttribute("reg_no", login_map.get("reg_no"));
			session.setAttribute("course", login_map.get("course"));
			session.setAttribute("color_code", login_map.get("color_code"));
			session.setAttribute("access_level", login_map.get("access_level"));
			session.setAttribute("logged_in", "true");
			//Redirect to home_page
			write_log(request,login_map.get("user_id"));
			//Write log function call
			int access_level=Integer.parseInt(login_map.get("access_level"));
			System.out.println("Access Level: "+access_level);
			if(access_level==10)
				response.sendRedirect("/project_path/pages/home_page.jsp");
			else if(access_level==20)
				response.sendRedirect("/project_path/pages/teacher_homepage.jsp");
			System.out.println(login_map.get("name")+"is logging in");

			
			
		}
		else{
			//log in failed
			session.setAttribute("logged_in", false);
			response.sendRedirect("/project_path/pages/login_page.jsp");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
		//to store login log.
		//by Anand Srivastava on 6/8/15 // user_ id & login_id
		public static void write_log(HttpServletRequest request, String user_id) throws SocketException, UnknownHostException, SQLException{
			
			InetAddress address = InetAddress.getLocalHost();
			NetworkInterface ni = NetworkInterface.getByInetAddress(address);
			byte[] mac = ni.getHardwareAddress();
			if (mac != null) {
	           String macHex = String.format("%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	           System.out.println(macHex);
	           System.out.println(address);
	           System.out.println(user_id);
	           new login();
	           project_path.Helper_Class.login.writeLog(user_id,address,macHex);
	           //login_servlet.login(request, response,user_name,password);
			}
			else{
				// MAC Error 
				System.out.println("Error/MAC Not Found");
			}
	    }
			
}
