package project_path.servelets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import project_path.Helper_Class.ProjectHelper;

/**
 * Servlet implementation class Add_member
 */
@WebServlet("/Add_member")
public class Add_member extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	ProjectHelper pro_help;
    public Add_member() {
        super();
        pro_help=new ProjectHelper();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pro_id=(String)request.getParameter("pro_id");
		String inviting_id=(String)request.getParameter("inviting_id");
		String invited_id=(String)request.getParameter("invited_id");
		
		pro_help.invite_members(pro_id, inviting_id, invited_id);
		System.out.println("Invited memebr "+pro_id+inviting_id+invited_id);
	}

}
