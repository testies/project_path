/*
 *Jonathan Fidelis Paul
 * 30-07-2015
 */
try{
	/*These functions are for the asynchronous check*/
var request;  
function sendInfo()  
{  
var r=document.reg_form.reg_no.value;  
var url="checkRegistration.jsp?reg_no="+r;  

if(window.XMLHttpRequest){  
request=new XMLHttpRequest();  
}  
else if(window.ActiveXObject){  
request=new ActiveXObject("Microsoft.XMLHTTP");  
}  
  
try{  
request.onreadystatechange=getInfo;  
request.open("GET",url,true);  
request.send();  
}
catch(e){
	alert("Unable to connect to server");
	}  
}  
  
function getInfo(){
	if(request.readyState==4){  
		var val=request.responseText;
		document.getElementById('message').innerHTML=val;
		if(val.search("already registered")>-1){
			document.getElementById('reg_no').value="";
			document.getElementById('reg_no').focus();
		}
	}
}
function checkNumber(n)
{
	for(i=0;i<n.length;i++)
	{
		if(!(n.charCodeAt(i)>=48&&n.charCodeAt(i)<=57))
			return 0;
	}
	return 1;
}

function validateRegisterNumber()
{
	var reg=document.getElementById('reg_no');
	if(reg.value.length==0)
		return;
	if((reg.value).length==7 && checkNumber(reg.value)==1 )
		{
			sendInfo();
		}
	else
		{
			document.getElementById('message').innerHTML="invalid register number";
			reg.value="";
			reg.focus();
		}
}
/*Functions for email validation*/
function validateMail()
{
	var mail=document.getElementById('email');
	var val=mail.value;
	if(val.length==0)
		return;
	if(val.length>20)
	{
		if((val.substr(val.length-19,19)).search("christuniversity.in")==0)
		{
			document.getElementById('message').innerHTML="register here";
		}
		else
		{
			document.getElementById('message').innerHTML="use your university-id";
			mail.value="";
			mail.focus();
		}
	}
	else
	{
		document.getElementById('message').innerHTML="use your university-id";
		mail.value="";
		mail.focus();
	}
	
}
}//End of try
catch(e)
{
	alert(e.message);
	}