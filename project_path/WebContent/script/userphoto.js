/*
 *Jonathan Fidelis Paul
 * 30-07-2015
 * This is used to manipulate the user's photo
 */
try{
	var tr;
	var canvas;
	var context;
	var im;
	var imageLoader;
	var xs=0;
	var ys=0;
	var scale=1;
	var px = 0;
	var py = 0;
	var cx = 0;
	var cy = 0;
	var prev_xs=xs;
	var prev_ys=ys;
	var prev_scale=scale;
	var width=210;
	var height=210;
	var W;
	var H;
	
function init()
{
    im = new Image();
	prev_xs=xs;
	prev_ys=ys;
	prev_scale=scale;
	
    canvas = document.getElementById("canvas");
	context = canvas.getContext("2d");
	im.src="../images/dp.jpg";
	imageLoader= document.getElementById('imageLoader');
	imageLoader.addEventListener('change', handleImage, false);
	
	im.onload = function(){
        context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
        W=im.width;
        H=im.height;
        storeImageData();
    }
    
	canvas.onmousedown=function(e)
	{
		tr=1;
		px=e.clientX;
		py=e.clientY;
	}
	canvas.onmousemove=function(e)
	{
	if(tr==1)
	    {
	    var tx=xs-(e.clientX-px);
	    var ty=ys-(e.clientY-py);
	        if(tx>0&&ty>0&&(tx+width*scale)<W&&(ty+height*scale)<H)
	        {
	            cx=e.clientX;
	            cy=e.clientY;
	            xs=xs-(cx-px);
	            ys=ys-(cy-py);
	            px=cx;
	            py=cy;
	            context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
	        }
	    }
	}
	canvas.onmouseup=function(e)
	{
		tr=0;
	}/*
	canvas.onmouseover=function(e){
		document.getElementById('edit_icon').style.visibility='visible';
	}
	canvas.onmouseout=function(e){
		document.getElementById('edit_icon').style.visibility='hidden';
	}*/
}//End of init

function returnToNormal()//This function returns the canvas to the previous state
{
	//   context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
	xs=prev_xs;
	ys=prev_ys;
	scale=prev_scale;
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
    im.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    storeImageData();
}

function hideDiv()
{
	document.getElementById('popup_div').style.visibility='hidden';
	document.getElementById('close_div').style.visibility='hidden';
	document.getElementById('popup_div').removeChild(canvas);
	document.getElementById('dp_div').appendChild(canvas);
	returnToNormal();
	context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
}
function showDiv()
{
	document.getElementById('close_div').style.visibility='visible';
	document.getElementById('popup_div').style.visibility='visible';
	document.getElementById('dp_div').removeChild(canvas);
	document.getElementById('popup_div').appendChild(canvas);
}
function zoomIn()
{
	scale=scale-0.1;
	context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
}

function zoomOut()
{
	scale=scale+0.1;
	context.drawImage(im,xs,ys,width*scale,height*scale,0,0,width,height);
}

function crop()
{
	document.getElementById('popup_div').style.visibility='hidden';
	document.getElementById('close_div').style.visibility='hidden';
	document.getElementById('popup_div').removeChild(canvas);
	document.getElementById('dp_div').appendChild(canvas);
	storeImageData();
}
function storeImageData()
{
	var dat=document.getElementById('canvasData');
	dat.value=canvas.toDataURL('image/png');
	prev_xs=xs;
	prev_ys=ys;
	prev_scale=scale;
}
}//end of try
catch(e)
{
	alert(e.message);
}