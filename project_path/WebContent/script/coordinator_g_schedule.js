/*
 *Jonathan Fidelis Paul
 *07-08-2015
 *This script handles issues in coordinator_g_schedule.jsp 
 * */
function add_phase_row()
	{
		try{
			var tab=document.getElementById("schedule_table");
			var count=document.getElementById("count");
			var row=document.createElement('tr');

			var cell=document.createElement('td');
			var inp=document.createElement('input');
			inp.placeholder="phase-name";
			inp.className="in_form";
			inp.name="phase_name"+count.value;
			inp.required=true;
			cell.appendChild(inp);
			row.appendChild(cell);

			var cell=document.createElement('td');
			var inp=document.createElement('input');
			inp.type="date";
			inp.placeholder="yyyy-mm-dd";
			inp.className="in_form";
			inp.name="start_date"+count.value;
			inp.required=true;
			cell.appendChild(inp);
			row.appendChild(cell);

			var cell=document.createElement('td');
			var inp=document.createElement('input');
			inp.type="date";
			inp.placeholder="yyyy-mm-dd";
			inp.className="in_form";
			inp.name="end_date"+count.value;
			inp.required=true;
			cell.appendChild(inp);
			row.appendChild(cell);

			var cell=document.createElement('td');
			var inp=document.createElement('textarea');
			inp.style.marginTop="4px";
			inp.style.maxHeight="30px";
			inp.style.maxWidth="240px";
			inp.placeholder="this phase is about?";
			inp.className="in_form";
			inp.name="description"+count.value;
			inp.required=true;
			cell.appendChild(inp);
			
			var del=document.createElement('a');
			del.innerHTML="X";
			del.href="#";
			del.name="del"+count.value;
			del.onclick=function(e)
			{
				try{
					for(var i=1;i<tab.rows.length;i++)
					{
						var row = tab.rows[i];
		                var rowObj = row.cells[3].childNodes[1];
		                
		                if (rowObj.name == e.target.name) {
	                    	tab.deleteRow(i);
	                    	count.value=count.value-1;
	                    	if(count.value==0)
	                    		document.getElementById('submit').disabled=true;
	                    	resetNames(tab);
	                	}
					}
				}
				catch(e){alert(e.message);}
			}
			del.style.position="relative";
			del.style.left="30px";
			del.style.top="-10px";
			cell.appendChild(del);
			
			row.appendChild(cell);
			
			tab.appendChild(row);
			
			count.value=parseInt(count.value)+1;
			document.getElementById('submit').disabled=false;
			//alert(count.value);
		}
		catch(e)
		{
			alert(e.message);
		}
	}
function resetNames(tab)
{
	var count=document.getElementById('count').value;
	for(var i=1;i<tab.rows.length;i++)
	{
		var row = tab.rows[i];
		row.cells[0].childNodes[0].name="phase_name"+(i-1);
		row.cells[1].childNodes[0].name="start_date"+(i-1);
		row.cells[2].childNodes[0].name="end_date"+(i-1);
		row.cells[3].childNodes[0].name="description"+(i-1);
	}
}