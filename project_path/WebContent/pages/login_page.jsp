<!-- Diganta Das 21 jul 15 -->
<%@page import="project_path.Helper_Class.G_Strings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>project path - login</title>
	<link rel="stylesheet" type="text/css" href="../style/login_style.css">
</head>
<body>
	<div class="top_div">

	</div>
	<div class="main_div">
		<div class="left_div">
			<div id="box1"></div>
			<div id="box2"></div>
		</div>
		<div class="login_module">
			<div class="logo_div"></div>
			
			<!--<form action="http://localhost:8081/project_path/login_servlet" method="post"> -->
			<form action="<%=G_Strings.url %>/login_servlet" method="post">
				<input class="input_box" type="username" name="username" placeholder="  username" required><br>
				<input class="input_box" type="password" name="password" placeholder="  password" required><br>
				<input id="login_btn" class="input_box" type="submit" value="login">
			</form>
			<a href="<%=G_Strings.url %>/pages/registration.jsp"><button id="register_btn" class="input_box">register</button></a>
		</div>
		<div class="right_div">
			<div id="box4"></div>
			<div id="box3"></div>
		</div>
		<div class="bottom_div">
			<div id="box6"></div>
			<div id="box5"></div>
		</div>
		<div class="footer_div">
			<div>  <%=G_Strings.footer %> </div>
		</div>
	</div>

</body>
</html>