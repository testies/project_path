<!-- 
Jonathan Fidelis Paul
06-08-2015
This page is for the project coordinator to schedule a new project group 
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="project_path.Helper_Class.G_Strings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Teacher-homepage</title>
	<link rel="stylesheet" type="text/css" href="../style/page_template.css">
	<link rel="stylesheet" type="text/css" href="../style/coordinator_g_schedule.css"/>
	<script src="../script/coordinator_g_schedule.js" type="text/javascript"></script>
</head>
<body onload="document.getElementById('g_sch_form').reset()">

	<datalist id="categories">
	  <option value="RDBMS">
	  <option value="Computer Architecture">
	  <option value="Networking">
	  <option value="Mobile Application">
	  <option value="Researh">
	</datalist> 
	<div id="main_div">
		<div id="top_div">
			<div id="pp_logo"></div>
			<div class="login_details">hello <b>new user</b> <a href="#">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b>new user</b><br>???????</a></div>
				</div>
				<div id="icon_div">
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div id="content_div" class="any_box">
				<form id="g_sch_form" action="<%=G_Strings.url %>/set_grand_schedule" method="post">
				<table>
					<tr>
						<td><input size="45" placeholder="schedule-name" name="schedule_name"></td>
						<td><input size="45" placeholder="year-sem" name="year_sem"></td>
					</tr>
					<tr>
						<td>
						<select style="width:348px;" name="course">
						<option>MCA</option>
						<option>BCA</option>
						<option selected>MSc</option>
						<option>BSc</option>
						</select>
						</td>
						<td><input size="45" placeholder="category" list="categories" name="category"></td>
					</tr>
					<tr>
						<td colspan="2">
							<table id="schedule_table" border="0">
								<tr id="header"><td>phase</td><td>start-date</td><td>end-date</td><td>description</td></tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2"><input type="button" value="+" onclick="add_phase_row()"><input type="submit" id="submit" disabled value="Set Schedule"></td></tr>
				</table>
				<input id="count" name="count" type="hidden" value="0">
				</form>
			</div>
		</div>
		<div id="bottom_div">
			<label id="footer_note">Anand Srivastava, Diganta Das, Jonathan Paul. Christ University, MCA (2014-2017)</label>
		</div>
</div>
</body>
</html>