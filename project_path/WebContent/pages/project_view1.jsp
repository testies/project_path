<%@page import="java.sql.ResultSet"%>
<%@page import="project_path.Helper_Class.ProjectHelper"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="project_path.Helper_Class.G_Strings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
if(session.getAttribute("logged_in")!="true"){
	response.sendRedirect("/project_path/pages/login_page.jsp");
}
String user_id=(String)session.getAttribute("user_id");
String pro_id=(String)request.getParameter("pro_id");
//System.out.println("From jsp "+pro_id+" "+user_id);
ProjectHelper helper=new ProjectHelper();
if(helper.isprojectmember(pro_id, user_id)==0){
	response.sendRedirect(G_Strings.url+"/pages/home_page.jsp");
}
ResultSet result=helper.get_project_details(pro_id);
ResultSet files=helper.get_project_files(pro_id);
ResultSet members=helper.get_project_members_set(pro_id);
result.first();
files.beforeFirst();
String color_code=(String)session.getAttribute("color_code");
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>project path: add new project</title>
	<link rel="stylesheet" type="text/css" href="../style/project_view1_style.css">
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
	<script src="../script/jquery.min.js"></script>
	
</head>
<style>
.background_theme{
	background-color: <%=color_code %>;
}
.font_theme{
	color: <%=color_code %>;
}
</style>
<script>
$(document).ready(function(){
    $("#option_about").click(function(){
    	// alert("Clicked");
    	$("#files_div").fadeOut(200);
    	$("#upload_div").fadeOut(200);
    	$("#invite_div").fadeOut(200);
        $("#project_details").delay(200).fadeIn(200);
       
    });
    $("#option_files").click(function(){
    	$("#project_details").fadeOut(200);
    	$("#upload_div").fadeOut(200);
    	$("#invite_div").fadeOut(200);
        $("#files_div").delay(200).fadeIn(200);
       
    });
    $("#option_upload").click(function(){
    	$("#project_details").fadeOut(200);
    	$("#files_div").fadeOut(200);
    	$("#invite_div").fadeOut(200);
        $("#upload_div").delay(200).fadeIn(200);
    });
     $("#option_invite").click(function(){
    	$("#project_details").fadeOut(200);
    	$("#files_div").fadeOut(200);
    	$("#upload_div").fadeOut(200);
        $("#invite_div").delay(200).fadeIn(200);
    });
});
</script>
<body>
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<a href="<%=G_Strings.url %>/pages/home_page.jsp"><div id="pp_logo"></div></a>
			<div class="login_details font_theme">hello <b><%= session.getAttribute("name")%></b> <a class="font_theme" href="<%=G_Strings.url %>/logout_servlet">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col background_theme">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b><%= session.getAttribute("name")%></b><br><%= session.getAttribute("reg_no")%></a></div>
				</div>
				<div id="icon_div">
					<a href="<%=G_Strings.url %>/pages/home_page.jsp">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div id="content_col">
				<div id="top_options">
					<div class="options_outer background_theme">
						<a id="option_about" href="#project_details"><div class="option_inner icon_about">about</div></a>
					</div>
					<div class="options_outer background_theme">
						<a id="option_files" href="#files_div"><div class="option_inner icon_files">files</div></a>
					</div>
					<div class="options_outer background_theme">
						<a id="option_upload" href="#upload_div"><div class="option_inner icons_upload">upload file</div></a>
					</div>
					<div class="options_outer background_theme">
						<a id="option_invite" href="#invite_div"><div class="option_inner icons_invite">invite</div></a>
					</div>
					
				</div>
				<div id="project_details" class="selection_div">
					<div class="project_title font_theme"><%=result.getString("title") %></div>
					<div class="project_cat"><%=result.getString("category") %></div>
					<div class="project_progress">
						<div class="bacground_progress"></div>
						<div class="real_progress background_theme" style="width:255px"></div>
						<!-- pixel size out of 510px percentage -->
					</div>
					<div class="number_progress font_theme">50%</div>
					<div class="project_abstruct">
						<%=result.getString("description") %>
					</div>
				</div>
				<div id="files_div" class="selection_div">
					<div id="file_list">
					<%while(files.next()){ %>
						<div class="file_holder">
							<div class="file_details">
								<a href="#"><div class="file_name2"><%=files.getString("fname") %></div></a>
								<div class="file_by2">by <%=files.getString("uname") %></div>
								<div class="file_date2"><%=files.getString("date") %></div>
							</div>
							<a href="#" class="file_btn btn_download background_theme"></a>
							<a href="#" class="file_btn btn_delete background_theme"></a>
						</div>
					<%} %>
						<!--<div class="file_holder">
							<div class="file_details">
								<a href="#"><div class="file_name2">SomeDoc.docx</div></a>
								<div class="file_by2">by diganta das</div>
								<div class="file_date2">25 July 2015</div>
							</div>
							<a href="#" class="file_btn btn_download"></a>
							<a href="#" class="file_btn btn_delete"></a>
						</div>-->
					</div>
				</div>
				<div id="upload_div" class="selection_div" >
					<form enctype="multipart/form-data" method="post" action="<%=G_Strings.url %>/File_Upload">
						<input type="file" name="file" id="file" class="hidden">
					</form>
					<div id="dropzone_outer">
						<a href="#" id="dropzone_a"><div class="dropzone" id="dropzone">
							drop file here
						</div></a>
					</div>
					<a href="#"><div id="upload_btn">Upload</div></a>
				</div>
				<div id="invite_div" class="selection_div">
					<h1>invite</h1>
				</div>
			</div>
			<div id="side_col">
				<div id="member_col">
					<a href="#" class="invite_icon" id="invite_mem">
						<div>invite memebers</div>
					</a>
					<%while(members.next()){ %>
					<a href="#">
						<div class="member_div">
							<div class="member_name"><%=members.getString("name") %></div>
							<div class="member_image" style="background-image: url(../images/dp.jpg); height: 50px; width: 50px;"></div>
						</div>
					</a>
					<%} %>
					<!--<a href="#">
						<div class="member_div">
							<div class="member_name">manvinder rathore</div>
							<div class="member_image" style="background-image: url(../images/pro_pic2.jpg); height: 50px; width: 50px;"></div>
						</div>
					</a>
					<a href="#">
						<div class="member_div">
							<div class="member_name">ritesh</div>
							<div class="member_image" style="background-image: url(../images/pro_pic3.jpg); height: 50px; width: 50px;"></div>
						</div>
					</a>-->
				</div>
				<div id="recent_activity">
					<div class="activity">diganta das completed the project page view 1 module, 15 jul 15</div>
					<div class="activity">anand srivastava uploaded abstruct document, 25 jul 15</div>
					<div class="activity">manvinder started with project view2 module, 16 jul 15</div>
					<div class="activity">jonathan is still thinking what to do, 1 aug 15</div>
				</div>
			</div>
		</div>
		<!-- <div id="bottom_div">
			<label id="footer_note">Anand Srivastava, Diganta Das, Jonathan Paul. Christ University, MCA (2014-2017)</label>
		</div> -->
	</div>
</body>
<!-- <script src="../script/dragdrop.js"></script> -->
<script type="text/javascript">
(function(){
	var dropzone=document.getElementById('dropzone');
	var upload_btn=document.getElementById('upload_btn');
	var input_file=document.getElementById('file');
	var formData=new FormData, xhr=new XMLHttpRequest,x;
	
	var attach = function(files){
		//for(x=0;x<files.length;x++){
		formData=new FormData;
		formData.append('file',files[0]);
		dropzone.innerHTML="file: "+files[0].name;
	};
	input_file.onchange=function(event){
		if(input_file.files.length==0)
			return;
		console.log(input_file.files);
		attach(input_file.files);
		$("#dropzone").animate({height: '100px'});
		//console.log(input_file.files);
	}
	$("#dropzone_a").click(function(){
		input_file.click();
		//console.log(input_file.files);
	});
	$("#upload_btn").click(function(){
		dropzone.innerHTML="uploading file";
		xhr.open('POST','<%=G_Strings.url %>/File_Upload?pro_id=<%=pro_id%>');
		xhr.send(formData);
		
		xhr.onload = function(){
			var data=this.responseText;
			if(data==0){
				dropzone.innerHTML="file uploading failed";
			}
			else if(data==2){
				dropzone.innerHTML="invalid file format, only bmp, png, jpg, jpeg, doc, docx, txt, pdf";
			}
			else{
				dropzone.innerHTML="file uploaded successfully";
			}
			console.log(data);
		}
	});
	dropzone.ondrop = function(e){
		e.preventDefault();
		this.className='dropzone';
		console.log(e.dataTransfer.files[0].name);
// 		dropzone.innerHTML="file: "+e.dataTransfer.files[0].name;
		attach(e.dataTransfer.files);
		$("#dropzone").animate({height: '100px'});
	};
	dropzone.ondragover = function(){
		$("#dropzone").animate({height: '200px'});
		this.className='dropzone dragover';
		return false;
	};
	dropzone.ondragleave = function(){
		$("#dropzone").animate({height: '100px'});
		this.className='dropzone';
		return false;
	};
	$("#invite_mem").click(function(){
		console.log("inviting members");
		$('<form class="hidden-form" action="add_member.jsp" method="post" style="display: none;"><textarea name="pro_id"></textarea><textarea name="user_id"></textarea><textarea name="search_param"></textarea></form>').appendTo('body');
    	$('[name="pro_id"]').val("<%=pro_id%>");
    	$('[name="user_id"]').val("<%=user_id%>");
    	$('[name="search_param"]').val("");
    	$('.hidden-form').submit();
	});
}());
</script>
</html>