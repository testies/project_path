<%@page import="project_path.Helper_Class.ProjectHelper"%>
<%@page import="java.sql.ResultSet" %>
<%@page import="project_path.Helper_Class.G_Strings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
if(session.getAttribute("logged_in")!="true"){
	response.sendRedirect("/project_path/pages/login_page.jsp");
}
String color_code=(String)session.getAttribute("color_code");
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>project path: add new project</title>
	<link rel="stylesheet" type="text/css" href="../style/project_home_style.css">
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
	<script src="../script/jquery.min.js"></script>
<style>
.background_theme{
	background-color: <%=color_code %>;
}
.font_theme{
	color: <%=color_code %>;
}
</style>
<script>
$(document).ready(function(){
    $("#current_icon_btn").click(function(){
    	$("#past_project_holder").fadeOut(200);
    	$("#invites_holder").fadeOut(200);
        $("#current_project_holder").delay(200).fadeIn(200);
       
    });
    $("#past_icon_btn").click(function(){
    	$("#current_project_holder").fadeOut(200);
    	$("#invites_holder").fadeOut(200);
        $("#past_project_holder").delay(200).fadeIn(200);
        
    });
    $("#invites_icon_btn").click(function(){
    	$("#current_project_holder").fadeOut(200);
    	$("#past_project_holder").fadeOut(200);
        $("#invites_holder").delay(200).fadeIn(200);
        
    });
});
</script>
</head>
<%
int user_id=0;
ProjectHelper pro_help=new ProjectHelper();
if(session.getAttribute("user_id")!=null){
	user_id=Integer.parseInt((String)session.getAttribute("user_id"));
}

ResultSet curr_result=pro_help.get_current_project(user_id);
ResultSet past_result=pro_help.get_past_project(user_id);
ResultSet invites_result=pro_help.get_project_invites(user_id);

curr_result.last();
int curr_count=curr_result.getRow();
past_result.last();
int past_count=past_result.getRow();
invites_result.last();
int invite_count=invites_result.getRow();
%>
<body>
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<a href="<%=G_Strings.url %>/pages/home_page.jsp"><div id="pp_logo"></div></a>
			<div class="login_details font_theme">hello <b><%= session.getAttribute("name")%></b> <a class="font_theme" href="<%=G_Strings.url %>/logout_servlet">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col background_theme">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b><%= session.getAttribute("name")%></b><br><%= session.getAttribute("reg_no")%></a></div>
				</div>
				<div id="icon_div">
					<a href="<%=G_Strings.url %>/pages/home_page.jsp">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="mid_col project_col">
				<table>
					<tr class="project_icon_row">
						<td><a id="current_icon_btn" href="#"><div class="project_icons current_icon background_theme"><div class="project_icon_text">current (<%=curr_count %>)</div></div></a></td>
						<td><a id="past_icon_btn" href="#"><div class="project_icons past_icon background_theme"><div class="project_icon_text">past (<%=past_count %>)</div></div></a></td>
						<td><a id="invites_icon_btn" href="#"><div class="project_icons invite_icon background_theme"><div class="project_icon_text">invites (<%=invite_count %>)</div></div></a></td>
						<td><a href="<%=G_Strings.url %>/pages/add_new_project_page.jsp"><div class="project_icons add_icon background_theme"><div class="project_icon_text">add</div></div></a></td>
						<td rowspan="2">
							<div class="upcoming_deadlines_div background_theme">
								upcoming deadlines
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div id="current_projects" class="current_project_div">
								<div id="current_project_holder">
									<%curr_result.beforeFirst();
									while(curr_result.next()){
										int pro_id=curr_result.getInt("project_id");
									%>
									<div  class="project_block_a background_theme">
										<a href="<%=G_Strings.url %>/pages/project_view1.jsp?pro_id=<%=pro_id%>">
<%-- 									<a href="<%=G_Strings.url %>/get_project_view?pro_id=<%=pro_id%>"> --%>
											<table class="project_block">
													<tr>
														<td colspan="2"><div class="project_title"><%=curr_result.getString("title")%></div></td>
													</tr>
													<tr>
														<td width="80%">
															<div class="project_cat"><%=curr_result.getString("category")%></div>
															<div class="project_member_name"><%=pro_help.get_project_members(pro_id) %></div>
														</td>
														<td>
															<div class="percentage">60%</div>
														</td>
													</tr>
											</table>
										</a>
									</div>
									<%} %>
								</div>
								<div id="past_project_holder">
									<div>
										<%
										past_result.beforeFirst();
										while(past_result.next()){
											int pro_id=past_result.getInt("project_id");
										%>
										<div class="project_block_a background_theme">
											<a href="#">
												<table class="project_block">
														<tr>
															<td colspan="2"><div class="project_title"><%=past_result.getString("title") %></div></td>
														</tr>
														<tr>
															<td width="100%">
																<div class="project_cat"><%=past_result.getString("category") %></div>
																<div class="project_member_name"><%=pro_help.get_project_members(pro_id) %></div>
															</td>
														</tr>
												</table>
											</a>
										</div>
										<%} %>
									</div>
								</div>
								<div id="invites_holder">
									<%
									invites_result.beforeFirst();
									while(invites_result.next()){
										int pro_id=invites_result.getInt("project_id");
									%>
									<div class="invite_block_outer background_theme" id="invite_<%=pro_id %>" >
										<table class="invite_block">
											<tr>
												<td colspan="2"><a class="invite_name" href="#"><div class="project_title"><%=invites_result.getString("title") %></div></a></td>
											</tr>
											<tr>
												<td width="70%">
													<div class="project_invited_by">by <%=invites_result.getString("name") %></div>
													<div class="project_cat"><%=invites_result.getString("category") %></div>
													<div class="project_member_name"><%=pro_help.get_project_members(pro_id) %></div>
												</td>
												<td>
													<a class="invite_name" href="#" onclick="invite_acccept(<%=pro_id %>,1)"><div class="invite_btn">accept</div></a>
													<a class="invite_name" href="#" onclick="invite_acccept(<%=pro_id %>,0)"><div class="invite_btn">reject</div></a>
												</td>
											</tr>	
										</table>
									</div>
									<%} %>
									
								</div>
							</div>
							
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="bottom_div">
			<label id="footer_note"> <%=G_Strings.footer %></label>
		</div>
	</div>
</body>
<script type="text/javascript">
function invite_acccept(pro_id, resp){	//response 0=reject, 1=accept
	console.log("Clciked "+pro_id);
	//$("#invite_9").slide(500);
	//$("#invite_"+pro_id).animate({ width: '0px', opacity: '0px', margin: '0px' }, 'medium');
	response(pro_id, resp);
}
var response=function(pro_id, resp){
	jQuery.ajax({
	    url:'<%=G_Strings.url %>/Invite_Response',
	    data:{"pro_id":pro_id, "user_id":"<%=user_id%>", "resp":resp},
	    type:'POST',
	    success:function(data, textStatus, jqXHR){
	    	//on sucess, hide that div
	    	$("#invite_"+pro_id).animate({ width: '0px', opacity: '0px', margin: '0px' }, 'medium');
	    },
	    error:function(data, textStatus, jqXHR){
	        console.log('Service call failed!');
	    }
	});
}
</script>
</html>
