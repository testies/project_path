<%@page import="java.sql.ResultSet"%>
<%@page import="project_path.Helper_Class.User_Helper"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="project_path.Helper_Class.G_Strings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
if(session.getAttribute("logged_in")!="true"){
	response.sendRedirect("/project_path/pages/login_page.jsp");
}
String color_code=(String)session.getAttribute("color_code");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>project path: add new project</title>
	<link rel="stylesheet" type="text/css" href="../style/add_member_style.css">
	<script src="../script/jquery.min.js"></script>
<style>
.background_theme{
	background-color: <%=color_code %>;
}
.font_theme{
	color: <%=color_code %>;
}
</style>
</head>
<%
String pro_id=request.getParameter("pro_id");
String user_id=request.getParameter("user_id");
String search_param=request.getParameter("search_param");

User_Helper u_help=new User_Helper();
ResultSet search_res=u_help.search_member(search_param,pro_id);
//search_res.beforeFirst();
%>
<body>
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<a href="<%=G_Strings.url %>/pages/home_page.jsp"><div id="pp_logo"></div></a>
			<div class="login_details font_theme">hello <b><%= session.getAttribute("name")%></b> <a class="font_theme" href="<%=G_Strings.url %>/logout_servlet">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col background_theme">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b>diganta das</b><br>1425913</a></div>
				</div>
				<div id="icon_div">
					<a href="<%=G_Strings.url %>/pages/home_page.jsp">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div id="search_div">
				<div id="search_bar">
					<input id="search_param" type="text" class="search_input" placeholder=" name or registration or email">
					<a href="#" onclick="search_init()"><div class="search_btn">search</div></a>
					<a href="#" onclick="go_back()"><div class="search_btn" id="icon_back">back</div></a>
				</div>
				<div id="notification_holder">
					<div id="notification">you have sent invitation to jonathan paul</div>
				</div>
				<div id="search_result">
				<%while(search_res.next()){ %>
					<div class="result_holder background_theme">
						<div class="result_image" style="background-image: url(../images/pro_pic2.jpg);"></div>
						<div class="result_name"><%=search_res.getString("name") %></div>
						<div class="result_dept"><%=search_res.getString("course") %></div>
						<a href="#" onclick="add_click('<%=search_res.getString("user_id") %>','<%=search_res.getString("name") %>')"><div class="result_add"></div></a>
					</div>
				<%} %>
				</div>
			</div>
		</div>
		<div id="bottom_div">
			<label id="footer_note">Anand Srivastava, Diganta Das, Jonathan Paul. Christ University, MCA (2014-2017)</label>
		</div>
	</div>
</body>
<script type="text/javascript">
function add_click(id,name){
	var notify=document.getElementById("notification");
	notify.innerHTML=name+" has been invited"
	$('#notification').fadeIn(300).delay(3000).fadeOut(300);
	
	var xhr=new XMLHttpRequest;
	var formData=new FormData;
	console.log("Pro id=<%=pro_id%> inviting_id="+id+"sender_id <%=user_id%>");
	
	jQuery.ajax({
	    url:'<%=G_Strings.url %>/Add_member',
	    data:{"pro_id":"<%=pro_id%>","invited_id":id,"inviting_id":"<%=user_id%>"},
	    type:'POST',
	    success:function(data, textStatus, jqXHR){
	        // access response data
	    },
	    error:function(data, textStatus, jqXHR){
	        console.log('Service call failed!');
	    }
	});
}
function search_init(){
	var search_box=document.getElementById("search_param");
	var param=search_box.value;
	console.log(param);
	$('<form class="hidden-form" action="add_member.jsp" method="post" style="display: none;"><textarea name="pro_id"></textarea><textarea name="user_id"></textarea><textarea name="search_param"></textarea></form>').appendTo('body');
	$('[name="pro_id"]').val("<%=pro_id%>");
	$('[name="user_id"]').val("<%=user_id%>");
	$('[name="search_param"]').val(param);
	$('.hidden-form').submit();
}
$("#search_param").keyup(function (e) {
    if (e.keyCode == 13) {
        search_init();
    }
});
function go_back(){
	console.log("Going back");
	window.history.back();
}
</script>
</html>