<!-- Diganta Das 23/Jul/15-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="project_path.Helper_Class.G_Strings"%>
<%
if(session.getAttribute("logged_in")!="true"){
	response.sendRedirect("/project_path/pages/login_page.jsp");
}
String color_code=(String)session.getAttribute("color_code");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>project path: add new project</title>
	<link rel="stylesheet" type="text/css" href="../style/add_project_page_style.css">
<style>
.background_theme{
	background-color: <%=color_code %>;
}
.font_theme{
	color: <%=color_code %>;
}
</style>
</head>
<datalist id="categories">
  <option value="RDBMS">
  <option value="Computer Arcitecture">
  <option value="Networking">
  <option value="Mobile Application">
  <option value="Reseearh">
</datalist> 
<body>
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<a href="<%=G_Strings.url %>/pages/home_page.jsp"><div id="pp_logo"></div></a>
			<div class="login_details">hello <b><%= session.getAttribute("name")%></b> <a href="<%=G_Strings.url %>/logout_servlet">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col background_theme">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b><%= session.getAttribute("name")%></b><br><%= session.getAttribute("reg_no")%></a></div>
				</div>
				<div id="icon_div">
					<a href="<%=G_Strings.url %>/pages/home_page.jsp">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div id="form_div" class="mid_col">
				<h1>add new project</h1>
				<form action="<%=G_Strings.url %>/new_project" method="post">
					<table>
						<tr>
							<td><div class="input_tags">actual project name</div>
							<input type="text" name="name" placeholder=" project name *" required></td>
						</tr>
						<tr>
							<td><div class="input_tags">project description, overview of the project</div>
							<textarea class="description_input" name="desc" placeholder=" description *" required></textarea></td>
						</tr>
						<tr>
							<td><div class="input_tags">project category, eg. RDBMS, Networking</div>
							<input name="category" list="categories" placeholder=" category *" required></td>
						</tr>
						<tr>
							<td><div class="input_tags">github, bitbucket, googledrive link for open public download</div>
							<input type="text" name="github_link" placeholder=" project link"></td>
						</tr>
						<tr>
							<td><div class="input_tags">search keywords</div>
							<input type="text" name="keywords" placeholder=" keywords"></td>
						</tr>
						<tr>
							<td><input type="submit" value="create" class="btn background_theme"></td>
						</tr>
					</table>
				</form>
			</div>
			<div id="side_div" class="mid_col">
				<h1>other project details!</h1>
			</div>
		</div>
		<div id="bottom_div">
			<label id="footer_note"> <%=G_Strings.footer %></label>
		</div>
	</div>
</body>
</html>