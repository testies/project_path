<!-- Diganta Das 21 jul 15 -->
<%@page import="project_path.Helper_Class.ProjectHelper"%>
<%@page import="java.sql.ResultSet" %>
<%@page import="project_path.Helper_Class.G_Strings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
int user_id=0;
if(session.getAttribute("logged_in")!="true"){
	response.sendRedirect("/project_path/pages/login_page.jsp");
}
int access_level=Integer.parseInt((String)session.getAttribute("access_level"));
if(access_level>=20){
	response.sendRedirect("/project_path/pages/teacher_homepage.jsp");
}
String color_code=(String)session.getAttribute("color_code");
ProjectHelper pro_help=new ProjectHelper();
if(session.getAttribute("user_id")!=null){
	user_id=Integer.parseInt((String)session.getAttribute("user_id"));
}
ResultSet curr_pro=pro_help.get_current_project(user_id);
curr_pro.beforeFirst();
%>
<html>
<head>
	<title>Project Path: Home</title>
	<link rel="stylesheet" type="text/css" href="../style/home_page_style.css">
<style>
.background_theme{
	background-color: <%=color_code %>;
}
.font_theme{
	color: <%=color_code %>;
}
</style>
</head>
<body>
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<a href="<%=G_Strings.url %>/pages/home_page.jsp"><div id="pp_logo"></div></a>
			<div class="login_details font_theme">hello <b><%= session.getAttribute("name")%></b> <a class="font_theme" href="<%=G_Strings.url %>/logout_servlet">(logout)</a></div>
			<div id="cu_logo"></div>
		</div>
		<div id="middle_div">
			<div class="mid_col pro_col background_theme">
				<div id="dp_div">
					<div id="dp_name"><a href="#"><b><%= session.getAttribute("name")%></b><br><%= session.getAttribute("reg_no")%></a></div>
				</div>
				<div id="icon_div">
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img dashboard_icon"></div>
						</div>
					</a>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp">
						<div class="icon_holder">
							<div class="icon_img project_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img calender_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img friends_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img discussion_icon"></div>
						</div>
					</a>
					<a href="#">
						<div class="icon_holder">
							<div class="icon_img logout_icon"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="mid_col col2">
				<div id="project_div" class="col2_div background_theme">
					<!-- This is project row 1 -->
					<%
					int nos=0;
					while(curr_pro.next() && nos<3){
						nos++;
						int pro_id=curr_pro.getInt("project_id");
					%>
					<a href="<%=G_Strings.url %>/pages/project_view1.jsp?pro_id=<%=pro_id%>">
						<div class="project_row odd">
							<div class="pie_holder">
								<div id="pieSlice1" class="hold"><div class="pie" style="transform:rotate(180deg);"></div></div>
								<div id="pieSlice2" class="hold"><div class="pie" style="transform:rotate(10deg);"></div></div>
								<div class="pie_percentage">60%</div>
							</div>
							<div class="project_details_holder">
								<div class="project_name"><%=curr_pro.getString("title") %></div>
								<div class="project_cat"><%=curr_pro.getString("category") %></div>
								<div class="project_mem"><%=pro_help.get_project_members(pro_id) %></div>
							</div>
						</div>
					</a>
					<%} %>
					<a href="<%=G_Strings.url %>/pages/myproject_page.jsp" class="project_more">more projects</a>
				</div>
				<div id="scrum_div" class="col2_div background_theme">
					<div id="scrum_head">Working Modules</div>
					<div id="scrum_stories">
						<a href="#"></a>
						<div class="story">
							<div class="story_name">LoginPage HTML</div>
							<div class="story_date">Started on 20/06/15</div>
						</div>
						<a href="#">
							<div class="story">
								<div class="story_name">Databse Table Creation</div>
								<div class="story_date">Started on 20/06/15</div>
							</div>
						</a>
						<a href="#">
							<div class="story">
								<div class="story_name">ERD Diagram</div>
								<div class="story_date">Started on 20/06/15</div>
							</div>
						</a>
						<a href="#">
							<div class="story">
								<div class="story_name">Login Module</div>
								<div class="story_date">Started on 20/06/15</div>
							</div>
						</a>
						<a href="#">
							<div class="story">
								<div class="story_name">DFD Diagram 2nd & 3rd Level</div>
								<div class="story_date">Started on 20/06/15</div>
							</div>
						</a>
					</div>
					<a href="#" class="project_more">more modules</a>
				</div>
			</div>
			<div class="mid_col background_theme">
				<div id="notification_col">
					<a href="#">
						<div class="notification odd">Virtual Food Management: Anand Shrivastava uploaded DFD</div>
					</a>
					<a href="#">
						<div class="notification even">Traffic Management System: Jonathan completed Login Module</div>
					</a>
					<a href="#">
						<div class="notification odd">Traffic Management System: Roseline Mary posted in discussion</div>
					</a>
					<a href="#">
						<div class="notification even">Project Path: Diganta Das completed HomePage HTML mock, and lots of other stuff, which i dont know. I am just writting this to check the overflow and underflow status of this div. Hope this works</div>
					</a>
					<a href="#">
						<div class="notification odd">Anand Shrivastav has submitted the DFD diagram in Virtual Food Management</div>
					</a>
					<a href="#">
						<div class="notification even">This is very small</div>
					</a>
				</div>
				<!-- <div>notification</div> -->
				<a href="#" class="project_more">more notifications</a>
			</div>
			<div class="mid_col background_theme">
				<div id="discussion_col">
					<a href="#">
						<div class="qs_cell">
							<div class="qs_div">How to call a servelet from Javascript?</div>
							<div class="by_div">by Kamath Ashish (14 replies)</div>
							<div class="response_div"><b>Diganta Das:</b> You should use AJAX to call servelet</div>
							<div class="response_div"><b>Sandip Dey:</b> Why you using Javascript?</div>
						</div>
					</a>

					<a href="#">
						<div class="qs_cell even">
							<div class="qs_div">What is the best way to do animations in HTML?</div>
							<div class="by_div">by Sindhu Audit (10 replies)</div>
							<div class="response_div"><b>Diganta Das:</b> use CSS animation</div>
							<div class="response_div"><b>Jonathan Paul:</b>You can use ajax library for interactive animations</div>
							<div class="response_div"><b>Anand Shrivastav:</b>You can use ajax library for interactive animations. I just want to write something really bug to check overflow status. This is not big enough, so i need to write more. I have no idea what i am writting, but i am still writting. If you reading this then you will be thinking that i am really stoned, and i guess so are you. I think i should stop this now.</div>
						</div>
					</a>

					<a href="#">
						<div class="qs_cell">
							<div class="qs_div">How to call a servelet from Javascript?</div>
							<div class="by_div">by Kamath Ashish (14 replies)</div>
							<div class="response_div"><b>Diganta Das:</b> You should use AJAX to call servelet</div>
							<div class="response_div"><b>Sandip Dey:</b> I just want?</div>
						</div>
					</a>
				</div>
				<a href="#" class="project_more">more discussions</a>
			</div>
		</div>
		<div id="bottom_div">
			<label class="font_theme" id="footer_note"> <%=G_Strings.footer %></label>
		</div>
	</div>
</body>
</html>