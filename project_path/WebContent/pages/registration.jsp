<!-- 
Date:25/07/2015
By  :Jonathan Fidelis Paul
This is the registration page
-->
<%@page import="project_path.Helper_Class.G_Strings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Project Path: Home</title>
	<script type="text/javascript" src="../script/userphoto.js"></script>
	<script type="text/javascript" src="../script/register_auth.js"></script>
	<link rel="stylesheet" type="text/css" href="../style/registration_style.css">
</head>
<body onload="init()">
	<!-- <meta http-equiv="refresh" content="2" > -->
	<div id="main_div">
		<div id="top_div">
			<div id="pp_logo"></div>
			<div id="cu_logo"></div>
		</div>
		
		<div id="middle_div">
			<table id="main_table">
				<tr>
					<td>
						<div id="pic_upload_div">
							<div id="dp_div" >
								<canvas id="canvas"  width="210" height="210" style="display:block;"></canvas>
							</div>
							
						</div>
							<form name="reg_form" enctype="multipart/form-data" action="<%=G_Strings.url+"/registration_servlet"%>" method="post">
							<input type="file" id="imageLoader" name="imageLoader"/>
							<input type="hidden" id="canvasData" name="canvasData" value="">
					</td>
					<td>
						<div id="form_div">
								<table>
								<tr><td><label id="message">Register Here</label></td></tr>
									<tr>
										<!-- <td>Registration Number: </td> -->
										<td><input type="text" id="reg_no" name="reg_no" placeholder=" registration number" onblur="validateRegisterNumber()" required></td>
									</tr>
									<tr>
										<td><input type="password" name="password" placeholder=" password" required></td>
									</tr>
									<tr>
										<td><input type="text" name="name" placeholder=" full name" required></td>
									</tr>
									<tr>
										<td>
											<select name="course">
												<option value="MCA">MCA</option>
												<option value="MSc">MSc</option>
												<option value="BCA">BCA</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><input type="email" id="email" name="email" placeholder=" email id" onblur="validateMail()" required></td>
									</tr>
									<tr>
										<td><input class="submit_btn" type="submit" value="register"></td>
									</tr>
								</table>
							</form>
							
							<div id="edit_icon" onclick="showDiv()"></div>
						</div>
						
					</td>
				</tr>
			</table>
			
		</div>
		
						<div id="close_div" onclick="hideDiv()"></div>
						<div id="popup_div" >
							<div id="zoomin" onclick="zoomIn()"></div>
							<div id="zoomout" onclick="zoomOut()"></div>
							<div id="crop" onclick="crop()"></div><br>
						</div>
		<div id="bottom_div">
			<label id="footer_note">Anand Srivastava, Diganta Das, Jonathan Paul. Christ University, MCA (2014-2017)</label>
		</div>
	</div>
</body>
</html>