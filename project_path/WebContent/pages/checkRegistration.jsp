<!-- 
 * Jonathan Fidelis Paul
 * 30-07-2015
 * This page is used by the registration.jsp to validate the user input
 * -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="project_path.Helper_Class.DB_Con" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.PreparedStatement" %>
<%  
String s=request.getParameter("reg_no");  
if(s==null || s.trim().equals("")){  
out.print("please enter here");  
}
else{
int reg_no=Integer.parseInt(s);
try{
DB_Con db=new DB_Con();
PreparedStatement ps=db.conn.prepareStatement("select name from user where user_name=?");  
ps.setString(1,""+reg_no);
ResultSet rs=ps.executeQuery();

int tr=0;
while(rs.next()){  
	tr++;
}
if(tr==0)
{
	out.print("register here");  	
}
else
{
	out.print("already registered");  		
}
db.close_db();  
}catch(Exception e){e.printStackTrace();}  
}  
%> 